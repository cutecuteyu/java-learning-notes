package com.studyjava.arraystudy;

import java.util.Arrays;

public class 数组的使用 {
    public static void main(String[] args) {
        int[] i2 = {1,2,3,4,5,6,7};
        int j[][] ={{1,2}, {3,4},{3,4}};
        //二维数组


        int[] num2;
        num2 = new int[10];
        for (int x=0 ; x<i2.length ; x++) {
            System.out.println("内容是："+i2[x]);
        }


        Arrays.fill(num2,2);
        //array.fill必须要新建对象的方式

        num2[0] = 1;
        System.out.println("内容是："+Arrays.toString(num2));


        int[] oldarray = new int[] {3, 4, 5, 6, 7};// 原始数组
        int num = 2;   // 删除索引为 2 的元素，即删除第三个元素 5
        int[] newArray = new int[oldarray.length-1];// 新数组，长度为原始数组减去 1

        for(int i=0;i<newArray.length; i++) {
            // 判断元素是否越界
            if (num < 0 || num >= oldarray.length) {
                throw new RuntimeException("元素越界... ");
            }
            //
            if(i<num) {
                newArray[i] = oldarray[i];
            }
            else {
                newArray[i] = oldarray[i+1];
            }
        }
        // 打印输出数组内容
        System.out.println(Arrays.toString(oldarray));
        oldarray = newArray;
        System.out.println(Arrays.toString(oldarray));
        //Java 的数组是固定长度的，无法直接删除，我们可以通过创建一个新数组，把原始数组中要保留的元素放到新数组中即可
    }
}
