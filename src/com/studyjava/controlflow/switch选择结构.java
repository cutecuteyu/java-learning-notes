package com.studyjava.controlflow;

public class switch选择结构 {
    public static void main(String[] args) {
        int i = 6;
        switch (i){
            case 1:
                System.out.println("结果是1");
                break;
            case 2:
                System.out.println("结果是2");
                break;
            case 3:
                System.out.println("结果是3");
                break;
            case 4:
                System.out.println("结果是4");
                break;
            case 5:
                System.out.println("结果是5");
                break;
            case 6:
                System.out.println("结果是6");
                break;
            default:
                System.out.println("结果是其他的");
                break;
        }
    }
}
