package com.studyjava.controlflow;

public class for循环 {
    public static void main(String[] args) {
        for (int i = 0 ; i<10 ; i++){
            System.out.println("输出"+i+"次");
        }
        int[] numbers = {1,2,3,4,5,6};
        for (int x:numbers){
            System.out.println("输出内容："+x);
        }
        //数组用冒号，冒号前面是元素，后面是数组
    }
}
