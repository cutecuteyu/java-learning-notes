package com.studyjava.controlflow;

import java.util.Scanner;

public class scannerstudy {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("输入框：");
        if (s.hasNext()){
            //如果存在输入
            String str = s.next();
            //赋值
            System.out.println("结果是："+str);
            //输出
        }
        s.close();
        //关闭接口

    }
}
