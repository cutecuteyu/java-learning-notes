package com.studyjava.controlflow;

import java.util.Scanner;

public class scannernextline {
    public static void main(String[] args) {
        Scanner s2 = new Scanner(System.in);
        System.out.println("输入框（nextline）：");
        String str2 = s2.nextLine();
        System.out.println("结果是："+str2);
        s2.close();
    }
}
//next去掉空格
//nextline以回车为准