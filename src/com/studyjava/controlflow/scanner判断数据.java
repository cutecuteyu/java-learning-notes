package com.studyjava.controlflow;

import java.util.Scanner;

public class scanner判断数据 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("输入框：");
        int i = 0;
        if (s.hasNextInt()){
            //如果存在输入
            i = s.nextInt();
            //赋值
            System.out.println("结果是："+i);
            //输出
        }
        else {
            System.out.println("请输入整数！");
        }
        s.close();
        //关闭接口
    }
}
