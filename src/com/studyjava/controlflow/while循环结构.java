package com.studyjava.controlflow;

public class while循环结构 {
    public static void main(String[] args) {
        int i = 0;
        while (i<6){
            System.out.println("输出"+i+"次");
            i++;
        }
        do {
            System.out.println("do输出"+i+"次");
            i++;
        }while (i<10);
    }
}
