package com.studyjava.controlflow;

public class if选择结构 {
    public static void main(String[] args) {
        int i = 3;
        if (i == 1){
            System.out.println("结果是1");
        }
        else if (i == 2){
            System.out.println("结果是2");
        }
        else {
            System.out.println("结果是其他的");
        }
        String a = "a";
        if (a.equals("a")){
            System.out.println("字符串是a");
        }
        else {
            System.out.println("字符串不是a");
        }
        //字符串用equals
    }
}
