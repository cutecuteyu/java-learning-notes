package com.studyjava.funcstudy;

public class 递归 {
    public static int f(int n){
        if (n==1){
            return 1;
        }else {
            return n*f(n-1);
            //自己调用自己，但是要注意一定要判断什么时候结束
        }
    }

    public static void main(String[] args) {
        System.out.println(f(5));
    }
}
