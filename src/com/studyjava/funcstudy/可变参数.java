package com.studyjava.funcstudy;

public class 可变参数 {
    public static void printmax(double... numbers){
        //...表示可变参数，需要对参数的长度和取值做操作，传进来的是数组
        if (numbers.length == 0){
            System.out.println("没有参数");
            return;
        }
        double result = numbers[0];
        for (int i=1;i<numbers.length;i++){
            if (numbers[i] > result){
                result = numbers[i];
            }
        }
        System.out.println("最大值是："+result);
    }
    public static void main(String[] args) {
        printmax(1,2,3,4,5,6);
    }
}
