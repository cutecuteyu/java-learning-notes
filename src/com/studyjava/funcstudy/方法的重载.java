package com.studyjava.funcstudy;

public class 方法的重载 {
    public static int add(int a,int b){
        int c =a+b;
        return c;
    }
    public static float add(float a,float b){
        float c = a+b;
        return c;
    }
    public static void main(String[] args) {
        //可以使用相同名字的函数 参数或者返回值不同
        int d = add(1,2);
        System.out.println("整数结果是："+d);
        float f = add(1.1f,1.2f);
        System.out.println("小数结果是："+f);
    }
}
