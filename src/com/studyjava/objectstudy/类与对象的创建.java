package com.studyjava.objectstudy;


public class 类与对象的创建 {
    public static void main(String[] args) {
    //一个项目应该只有一个main方法
        //实际类型是确定的，引用类型是可变的
        teacher s1 = new teacher();
        人 s2 = new teacher();
        //父类的引用指向子类的类型
        Object s3 = new teacher();
        //object也可以
        s2.say();
        //子类重写了父类的方法
        s1.say();

        //s2.eat() 报错
        ((teacher)s2).eat();
        //强制类型转换就可以
        s1.eat();
        //还是看左边的东西，和右边没有关系

    }
}
/**
 *         //一个项目应该只有一个main方法
 *         //实例化一个对象
 *         student xiaoming = new student();
 *         xiaoming.name = "小明";
 *         xiaoming.study();
 */

/**
 * 什么都不写的东西也有默认的东西就是构造器
 * //
 * // Source code recreated from a .class file by IntelliJ IDEA
 * // (powered by FernFlower decompiler)
 * //
 *
 * package com.studyjava.objectstudy;
 *
 * public class 构造器 {
 *     public 构造器() {
 *     //必须要有无参构造，如果存在有参构造那么必须要再写一个无参构造
 *     }
 * }
 *
 *
 *
 *         //实例化对象
 *         构造器 构造器 = new 构造器();
 */

/**
 * 私有属性的设置与获取
 *         student xiaohong = new student();
 *         xiaohong.setId(2);
 *         System.out.println(xiaohong.getId());
 */

/**
 * object
 *         teacher teacher = new teacher();
 *         teacher.say();
 *         //子类继承父类就会有全部公有方法和公有属性
 *         //teacher.getClass();
 *         //object类
 *
 */

/**
 * super
 *         teacher teacher = new teacher();
 *         teacher.getname("aaaa");
 *         //super方式和this方式
 *         //super无法取到private
 */

/**
 * 类的重写
 *         //重写都是方法的重写，和属性无关
 *         A a =new A();
 *         a.test();
 *         B b = new A();
 *         //父类的引用指向了子类
 *         b.test();
 *         //声明的时候的结构决定里面的方法调用哪个，也就是只和左边有关
 *         //去掉静态就全部走A，也就是子类重写了父类的方法
 */