package com.studyjava.objectstudy;

public class student {
    //属性
    String name;
    int age;
    private int id;
    //私有属性,提供私有属性的一些public方法用于访问与设置
    public int getId(){
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }


    //方法
    public void study(){
        System.out.println(this.name+"在学习中");
    }


}
