package com.studyjava.objectstudy;

class Person{

    String name;
}
//引用传递


public class 方法的回顾 {
    public static String hello(){
        return "hello";
    }
    public String hello2(){
        return "hello";
    }


    public static void change(Person person){
        person.name = "qqy";
    }
    //引用传递

    public static void main(String[] args) {
        System.out.println(hello());
        String a = new 方法的回顾().hello2();
        System.out.println(a);
        //非静态方法的使用

        Person person = new Person();
        System.out.println(person.name);
        方法的回顾.change(person);
        System.out.println(person.name);
        //引用传递
    }
}

